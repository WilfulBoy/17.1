﻿#include <iostream>
#include <math.h>

class Vector
{
private:
	double x;
	double y;
	double z;
public:
	Vector() : x(0), y(0), z(0)
	{

	}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{

	}
	void ShowCor()
	{
		std::cout << '\n' << x << ' ' << y << ' ' <<  z;
	}
	double GetLength()
	{
		return sqrt(x * x + y * y + z * z);
	}

	
};

int main()
{
	Vector v(10, 10, 10);
	v.ShowCor();
	v.GetLength();
	std::cout << ' ' << v.GetLength();
}
